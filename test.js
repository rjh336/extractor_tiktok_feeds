//'use strict';
const sinon = require('sinon');
const expect = require('chai').expect;
const {main} = require('.');
const {File} = require('@google-cloud/storage');

// Create mock Pub/Sub event
var jsonMessage = {
    entityType: 'TikTokUserFeed',
    entityId: 'spotify',
    options: {number: 10, useTestEndpoints: true
    }};


beforeEach(function() {
  sandbox = sinon.createSandbox();
  sandbox.spy(console, 'log');
  sandbox.spy(main.getFeed);
  sandbox.stub(File.prototype, "save");
});

afterEach(function() {
  sandbox.restore();
});


it('main: scrape user feed', () => {
  var event = {
      data: Buffer.from(JSON.stringify(jsonMessage)).toString('base64')
  };

  main(event);
  
  expect(
    console.log.calledWith(
      `Found ${jsonMessage.options.number} records for user @${jsonMessage.entityId}`
    )).to.be.true;

});


it('main: scrape hashtag feed', () => {
  jsonMessage['entityType'] = 'TikTokHashtagFeed';
  var event = {
      data: Buffer.from(JSON.stringify(jsonMessage)).toString('base64'),
  };
  main(event);
  expect(
    console.log.calledWith(
      `Found ${jsonMessage.options.number} records for hashtag #${jsonMessage.entityId}`
      )).to.be.true;
});


it('main: scrape trend feed', () => {
  jsonMessage['entityType'] = 'TikTokTrendFeed';
  jsonMessage['entityId'] = '';
  var event = {
      data: Buffer.from(JSON.stringify(jsonMessage)).toString('base64'),
  };
  main(event);
  expect(
    console.log.calledWith(
      `Found ${jsonMessage.options.number} records in trends`
      )).to.be.true;
});