const TikTokScraper = require('tiktok-scraper');
const { v4: uuidv4 } = require('uuid');
const {Storage} = require('@google-cloud/storage');

const storage = new Storage();
const bucket = storage.bucket(process.env.WRITE_STORAGE_BUCKET);


getFeed = async (entityType, entityId, options) => {
  try {
    let today = new Date(Date.now());
    let extractedAt = today.toISOString();

    switch(entityType) {
      
      case 'TikTokHashtagFeed':
        hashtagId = entityId.replace(/#/gi, '');
        console.log( `Scraping hashtag: #${hashtagId} with options ${JSON.stringify(options)}` );
        var posts = await TikTokScraper.hashtag(hashtagId, options);
        var postsFormatted = posts.collector.map(x => {
          return { id: x.id, _extracted_at: extractedAt, _json: x }
        });
        console.log( `Found ${postsFormatted.length} records for hashtag #${hashtagId}` );
        break;

      case 'TikTokUserFeed':
        userId = entityId.replace(/@/gi, '');
        console.log( `Scraping user: @${userId} with options ${JSON.stringify(options)}` );
        var posts = await TikTokScraper.user(userId, options); 
        var postsFormatted = posts.collector.map(x => {
          return { id: x.id, _extracted_at: extractedAt, _json: x }
        });
        console.log( `Found ${postsFormatted.length} records for user @${userId}` );
        break;

      case 'TikTokTrendFeed':
        console.log( `Scraping trend: ${entityId} with options ${JSON.stringify(options)}` );
        var posts = await TikTokScraper.trend(entityId, options);
        var postsFormatted = posts.collector.map(x => {
          return { id: x.id, _extracted_at: extractedAt, _json: x }
        });
        console.log( `Found ${postsFormatted.length} records in trends` );
        break;

      default:
        var err = entityType + ' is not a valid entity type';
        throw err;
    }
    return {
      entityType: entityType, 
      posts: postsFormatted
    };

    } catch (error) {
      console.log(error);
    }
  }

toCloudStorage = (data) => {
    try {
      if (data.length > 0) {
        // reformat to newline delimited json
        let fileContent = data.posts.map(JSON.stringify).join('\n');

        // create object name
        let today = new Date(Date.now());
        let todayDate = today.toISOString().slice(0, 10); 
        let objectName = data.entityType + '/' + todayDate + '/' + uuidv4() + '.json';

        // upload object to storage bucket
        const file = bucket.file(objectName);
        file.save(fileContent, function(err) {
            if (!err) {
            // File written successfully
            console.log('Wrote data to: gs://' + process.env.WRITE_STORAGE_BUCKET + '/' + objectName);
            }
        });
      }
    } catch (error) {
      console.log(error);
    }
}


/*  Message schema:
{
  entityType: 'TikTokUserFeed' | 'TikTokHashtagFeed' | 'TikTokTrendFeed',
  entityId: <string>,
  options: {
    https://github.com/drawrowfly/tiktok-scraper#options
  }
} */
exports.main = (message, context) => {
  const jsonMessage = JSON.parse(Buffer.from(message.data, 'base64').toString());
  return getFeed(jsonMessage.entityType, jsonMessage.entityId, jsonMessage.options)
    .then( response => { toCloudStorage(response) } );
};